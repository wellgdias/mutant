# Desafio Backend - Mutant

API REST desenvolvida em Node utilizando Typescript para o dev challenge da Mutant.

## Instalação

Será necessário possuir o vagrant instalado e instalar o plugin do docker-compose

```
vagrant plugin install vagrant-docker-compose
```

## Endpoints da API

Listar os websites de todos os usuários

```
GET /api/v1/website
```

Listar o Nome, email e a empresa em que trabalha (em ordem alfabética).

```
GET /api/v1/user
```

Listar todos os usuários que no endereço contem a palavra `suite`

```
GET /api/v1/address
```

## Dificuldades

- Vagrant: não conhecia o vagrant, tentei fazer a configuração da maquina virtual com um container da api, executei os comandos e a criação da VM com o ambiente docker foi criado, mas não consegi validar se está tudo funcionando.
- Docker - não possuo muito conhecimento sobre o docker, mas configurei a aplicação para ser executada no container.
