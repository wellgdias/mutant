import { Router } from 'express'
import { adapterRoute } from '../adapters/express-route-adapter'
import { makeAddressController } from '../factories/address-factory'
import { makeWebsiteController } from '../factories/website-factory'
import { makeUserController } from '../factories/user-factory'

export default (router: Router): void => {
  router.get('/address', adapterRoute(makeAddressController()))
  router.get('/website', adapterRoute(makeWebsiteController()))
  router.get('/user', adapterRoute(makeUserController()))
}
