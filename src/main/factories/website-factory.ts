import { WebSiteController } from '../../presentation/controllers/website-controller'
import { Controller } from '../../presentation/protocols'
import { ApiAdapter } from '../../infra/api/api-adapter'

export const makeWebsiteController = (): Controller => {
  const apiAdapter = new ApiAdapter()
  return new WebSiteController(apiAdapter)
}
