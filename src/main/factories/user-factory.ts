import { UserController } from '../../presentation/controllers/user-controller'
import { Controller } from '../../presentation/protocols'
import { ApiAdapter } from '../../infra/api/api-adapter'

export const makeUserController = (): Controller => {
  const apiAdapter = new ApiAdapter()
  return new UserController(apiAdapter)
}
