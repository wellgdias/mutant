import { AddressController } from '../../presentation/controllers/address-controller'
import { Controller } from '../../presentation/protocols'
import { ApiAdapter } from '../../infra/api/api-adapter'

export const makeAddressController = (): Controller => {
  const apiAdapter = new ApiAdapter()
  return new AddressController(apiAdapter)
}
