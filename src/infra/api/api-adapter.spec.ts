import { ApiAdapter } from './api-adapter'
import { UserModel, UserInfoModel } from '../protocols/user-model'

const makeSut = (): ApiAdapter => {
  return new ApiAdapter()
}

const makeUser = (): UserModel => {
  return {
    id: 2,
    name: 'Ervin Howell',
    username: 'Antonette',
    email: 'Shanna@melissa.tv',
    address: {
      street: 'Victor Plains',
      suite: 'Suite 879',
      city: 'Wisokyburgh',
      zipcode: '90566-7771',
      geo: {
        lat: '-43.9509',
        lng: '-34.4618'
      }
    },
    phone: '010-692-6593 x09125',
    website: 'anastasia.net',
    company: {
      name: 'Deckow-Crist',
      catchPhrase: 'Proactive didactic contingency',
      bs: 'synergize scalable supply-chains'
    }
  }
}

const makeUserInfo = (): UserInfoModel => {
  return {
    name: 'Chelsey Dietrich',
    email: 'Lucio_Hettinger@annie.ca',
    company: 'Keebler LLC'
  }
}

const makeWebsite = (): string[] => {
  return [
    'hildegard.org',
    'anastasia.net',
    'ramiro.info',
    'kale.biz',
    'demarco.info',
    'ola.org',
    'elvis.io',
    'jacynthe.com',
    'conrad.com',
    'ambrose.net'
  ]
}

describe('API Adapter', () => {
  test('Should return userInfo on call getUserInfo', async () => {
    const sut = makeSut()
    const getUser = await sut.getUserInfo()
    expect(getUser[0]).toStrictEqual(makeUserInfo())
    expect(getUser).toHaveLength(10)
  })

  test('Should return all website on call getUserWebsite', async () => {
    const sut = makeSut()
    const getWebsite = await sut.getUserWebsite()
    expect(getWebsite).toEqual(expect.arrayContaining(makeWebsite()))
  })

  test('Should return userInfo on call getUserAddress', async () => {
    const sut = makeSut()
    const getUserAddress = await sut.getUserAddress()
    expect(getUserAddress[0]).toStrictEqual(makeUser())
    expect(getUserAddress).toHaveLength(7)
  })
})
