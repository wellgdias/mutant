import fetch from 'node-fetch'
import { serverError } from '../../presentation/helpers/http-helper'
import { UserApi } from '../protocols/user-api'
import { UserModel, UserInfoModel } from '../protocols/user-model'

export class ApiAdapter implements UserApi {
  private readonly url = 'https://jsonplaceholder.typicode.com/users'
  async getApi(): Promise<UserModel[]> {
    return await fetch(this.url)
      .then(async (data) => await data.json())
      .catch((error) => serverError(error))
  }

  async getUserInfo(): Promise<UserInfoModel[]> {
    const result = await this.getApi()
    const userInfo = result
      .map((user) => ({
        name: user.name,
        email: user.email,
        company: user.company.name
      }))
      .sort(function (a, b) {
        if (a.name > b.name) {
          return 1
        }
        if (a.name < b.name) {
          return -1
        }
        return 0
      })
    return userInfo
  }

  async getUserWebsite(): Promise<string[]> {
    const result = await this.getApi()
    const userWebsites: string[] = result.map((user) => user.website)
    return userWebsites
  }

  async getUserAddress(): Promise<UserModel[]> {
    const result = await this.getApi()
    const users = result.filter((user) =>
      user.address.suite.toLowerCase().includes('suite')
    )
    return users
  }
}
