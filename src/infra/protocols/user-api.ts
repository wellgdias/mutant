import { UserInfoModel, UserModel } from './user-model'

export interface UserApi {
  getUserInfo(): Promise<UserInfoModel[]>
  getUserWebsite(): Promise<string[]>
  getUserAddress(): Promise<UserModel[]>
}
