import { AddressController } from './address-controller'
import { ApiAdapter } from '../../infra/api/api-adapter'

const makeApiAdapter = (): ApiAdapter => {
  return new ApiAdapter()
}

const makeSut = (): AddressController => {
  return new AddressController(makeApiAdapter())
}

describe('Address Controller', () => {
  test('Should return userInfo on call getUserAddress', async () => {
    const sut = makeSut()
    const userInfo = await sut.handle()
    expect(userInfo).toBeTruthy()
  })
})
