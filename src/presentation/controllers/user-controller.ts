import { serverError, ok } from '../helpers/http-helper'
import { Controller, HttpResponse } from '../protocols'
import { ApiAdapter } from '../../infra/api/api-adapter'

export class UserController implements Controller {
  constructor(private readonly apiAdapter: ApiAdapter) {}

  async handle(): Promise<HttpResponse> {
    try {
      const users = await this.apiAdapter.getUserInfo()
      return ok(users)
    } catch (error) {
      serverError(error)
    }
  }
}
