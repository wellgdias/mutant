import { WebSiteController } from './website-controller'
import { ApiAdapter } from '../../infra/api/api-adapter'

const makeApiAdapter = (): ApiAdapter => {
  return new ApiAdapter()
}

const makeSut = (): WebSiteController => {
  return new WebSiteController(makeApiAdapter())
}

describe('Address Controller', () => {
  test('Should return userInfo on call getUserWebsite', async () => {
    const sut = makeSut()
    const userInfo = await sut.handle()
    expect(userInfo).toBeTruthy()
  })
})
