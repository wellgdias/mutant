import { serverError, ok } from '../helpers/http-helper'
import { Controller, HttpResponse } from '../protocols'
import { ApiAdapter } from '../../infra/api/api-adapter'

export class AddressController implements Controller {
  constructor(private readonly apiAdapter: ApiAdapter) {}

  async handle(): Promise<HttpResponse> {
    try {
      const usersAddress = await this.apiAdapter.getUserAddress()
      return ok(usersAddress)
    } catch (error) {
      serverError(error)
    }
  }
}
