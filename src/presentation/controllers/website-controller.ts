import { serverError, ok } from '../helpers/http-helper'
import { Controller, HttpResponse } from '../protocols'
import { ApiAdapter } from '../../infra/api/api-adapter'

export class WebSiteController implements Controller {
  constructor(private readonly apiAdapter: ApiAdapter) {}

  async handle(): Promise<HttpResponse> {
    try {
      const userWebsite = await this.apiAdapter.getUserWebsite()
      return ok(userWebsite)
    } catch (error) {
      serverError(error)
    }
  }
}
