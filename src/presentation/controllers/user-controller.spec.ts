import { UserController } from './user-controller'
import { ApiAdapter } from '../../infra/api/api-adapter'

const makeApiAdapter = (): ApiAdapter => {
  return new ApiAdapter()
}

const makeSut = (): UserController => {
  return new UserController(makeApiAdapter())
}

describe('Address Controller', () => {
  test('Should return userInfo on call getUserInfo', async () => {
    const sut = makeSut()
    const userInfo = await sut.handle()
    expect(userInfo).toBeTruthy()
  })
})
